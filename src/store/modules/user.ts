import { defineStore } from 'pinia';

interface UserState {
    id?: string | number;
    userInfo: any
}

export const useUserStore = defineStore({
    id: 'user',
    state: (): UserState => ({
        id: 1,
        userInfo: {}
    }),
    getters: {},
    actions: {},
});
