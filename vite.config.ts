import { ConfigEnv, loadEnv, UserConfig } from "vite";
import uni from "@dcloudio/vite-plugin-uni";
import { resolve } from "path";
import UnoCSS from 'unocss/vite'
import AutoImport from 'unplugin-auto-import/vite'


// https://vitejs.dev/config/
export default ({ mode }: ConfigEnv): UserConfig => {
    const root = process.cwd();
    const env = loadEnv(mode, root);
    return {
        base: './',
        plugins: [
            uni(),
            UnoCSS(),
            AutoImport({
                imports: ['vue', 'uni-app'],
                dts: 'src/auto-import.d.ts',
                dirs: ['src/hooks'], // 自动导入 hooks
                eslintrc: { enabled: true },
            }),
        ],
        resolve: {
            alias: {
                '@': resolve('./src')
            },
            extensions: ['.js', '.json', '.ts', '.vue'], //使用路径别名时想要省略的后缀名,可自行增减
        },
        //自定义全局变量
        define: {
            'process.env': {},
        },
        // 开发服务器配置
        server: {
            host: true,
            // open: true,
            port: env.VITE_PORT as any,
            proxy: {
                '/api': {
                    target: env.VITE_BASE_URL,
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/api/, ''),
                },
                '/upload': {
                    target: env.VITE_BASE_URL,
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/upload/, ''),
                },
            },
        },


    }
}
